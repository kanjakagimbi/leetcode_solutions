def is_subsequence(s: str, t: str) -> bool:
    r = 0
    l = 0

    while r < len(s) - 1 and l < len(t) - 1:

        if s[r] == t[l]:
            r += 1

        l += 1
    return r == len(s)


s = "axc"
t = "ahbgdc"

print(is_subsequence(s, t))
