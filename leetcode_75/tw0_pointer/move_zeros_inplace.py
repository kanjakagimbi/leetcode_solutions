from typing import List


def move_zeros(nums: List[int]) -> int:
    l = 0
    for i, num in enumerate(nums):
        if num:
            nums[l], nums[i] = nums[i], nums[l]
            l += 1
    return nums


nums = [0, 1, 0, 3, 12]
print(move_zeros(nums))
