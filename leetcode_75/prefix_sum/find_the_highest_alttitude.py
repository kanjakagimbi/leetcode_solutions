from typing import List


def largest_altitude(gain: List[int]) -> int:
    res = [0] * (len(gain) + 1)
    for i, ele in enumerate(gain):
        res[i + 1] = res[i] + ele

    return res


gain = [-5, 1, 5, 0, -7]
print(largest_altitude(gain))
