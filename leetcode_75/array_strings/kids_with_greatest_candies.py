from typing import List
def kids_with_candies(candies: List[int], extra_candies: int) -> List[bool]:
    res = []
    n = max(candies)
    for x in candies:
        if x + extra_candies >= n:
            res.append(True)
        else:
            res.append(False)
    return res


