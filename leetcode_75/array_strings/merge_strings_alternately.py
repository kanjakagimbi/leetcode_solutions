def merge_alternately(word1: str, word2: str) -> str:
    bigg = max(word1, word2, key=len)
    n = min(len(word1), len(word2))
    remian = bigg[n:]
    res = ""
    for x, y in zip(word1, word2):
        res += x + y
    return res + remian


a = "ab"
b = "pqrs"

print(merge_alternately(a, b))
