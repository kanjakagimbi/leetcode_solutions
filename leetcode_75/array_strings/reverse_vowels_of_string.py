def reverse_vowels(s: str) -> str:
    vowels = "aeiouAEIOU"
    res = list(s)
    l = 0
    r = len(res) - 1

    while l < r:
        if res[l] not in vowels:
            l += 1

        if res[r] not in vowels:
            r -= 1

        res[l], res[r] = res[r], res[l]
        r -= 1
        l += 1

    return res


print(reverse_vowels("leetcode"))
