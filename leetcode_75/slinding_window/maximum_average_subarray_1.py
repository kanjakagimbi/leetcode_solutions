from typing import List


def find_max_average(nums: List[int], k: int) -> float:
    l = c = 0
    totals = 0
    avg = float("-inf")
    for x in nums:
        if c < k:
            totals += x
            print(totals)
            c += 1
        if c == k:
            avg = max(totals / k, avg)
            c -= 1
            totals -= nums[l]
            l += 1

    return avg


nums = [-1]
k = 1
print(find_max_average(nums, k))
n = [5]
print(sum(n[:1]))
